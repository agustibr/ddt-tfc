---
layout: page
title: About
permalink: /about/
---

This is the base DDT site.

Find out more: [DDT Blueprint](https://cryptpad.fr/pad/#/3/pad/edit/e557ee7efdcb293b02985c863a5ec9e8/)

You can find the source code of this site at:
[ddt-tfc](https://gitlab.com/agustibr/ddt-tfc).
